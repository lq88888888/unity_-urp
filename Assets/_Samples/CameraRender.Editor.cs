using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Rendering;

public partial class CameraRender
{
    partial void DrawGizmos();
    partial void PrepareForSceneWindow();
    partial void PrepareBuffer();
#if UNITY_EDITOR
    /// <summary>
    /// 绘制gizmos
    /// </summary>
    partial void DrawGizmos()
    {
        if(Handles.ShouldRenderGizmos())//检查是否应该绘制gizmos
        {
            //第二个参数来指示应该绘制哪个gizmo子集
            context.DrawGizmos(camera, GizmoSubset.PreImageEffects);
            context.DrawGizmos(camera, GizmoSubset.PostImageEffects);
            //有两个子集，用于图像效果的前和后
        }
    }
    /// <summary>
    /// 绘制Scene窗口中的UI
    /// </summary>
    partial void PrepareForSceneWindow()
    {
        if (camera.cameraType == CameraType.SceneView)
        {
            ScriptableRenderContext.EmitWorldGeometryForSceneView(camera);//绘制Scene窗口中的UI
        }
    }
    /// <summary>
    /// 更改缓冲区名称 
    /// </summary>
    partial void PrepareBuffer()
    {
        buffer.name = camera.name;
    }

#endif
}
