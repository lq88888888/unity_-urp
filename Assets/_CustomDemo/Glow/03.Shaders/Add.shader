﻿Shader "Custom/Add"
{
    Properties
    {
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		//_AddTex("Add",2D) = "white"{}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
		Pass
		{
			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct appdata
			{
				float4 vertex:POSITION;
				float2 uv:TEXCOORD;
			};
			struct v2f
			{
				float4 vertex:POSITION;
				float2 uv:TEXCOORD;
			};
			TEXTURE2D(_MainTex);
			SAMPLER(sampler_MainTex);
			sampler2D _AddTex;
			v2f vert(appdata v)
			{
				v2f o;
				o.uv = v.uv;
				o.vertex = TransformObjectToHClip(v.vertex);
				return o;
			}

			float4 frag(v2f i):SV_TARGET
			{
				return SAMPLE_TEXTURE2D(_MainTex,sampler_MainTex,i.uv);
				//float4 main = tex2D(_MainTex,i.uv);
				//float4 add = tex2D(_AddTex,i.uv);
				//return main+add;
			}
			ENDHLSL
		}
    }
    FallBack "Diffuse"
}
