﻿Shader "Custom/Depth"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
    }
    SubShader
    {
		Cull Off ZWrite Off ZTest Always
        LOD 200
		Pass
	{
		HLSLPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		struct appdata
		{
			float4 vertex:POSITION;
			float2 uv:TEXCOORD;
		};
		struct v2f
		{
			float4 pos:POSITION;
			float2 uv:TEXCOORD;
		};
		sampler2D _CameraDepthTexture;
		v2f vert(appdata i)
		{
			v2f o;
			o.pos = UnityObjectToClipPos(i.vertex);
			o.uv = i.uv;
			return o;
		}

		float4 frag(v2f i) :SV_TARGET
		{
			float depth = UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture,i.uv));
			float linearDepth = Linear01Depth(depth);

			return float4(linearDepth, linearDepth, linearDepth, 1);
		}

		ENDHLSL
			}
    }
    FallBack "Diffuse"
}
