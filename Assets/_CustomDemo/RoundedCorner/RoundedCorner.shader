Shader "SR/RoundedCorner"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Radius("Corner radius",Range(0,50)) = 20
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            //不要开启混合，否则透明窗口会花屏
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float2 pixel : TEXCOORD1;
            };
            
            sampler2D _MainTex;
            float4 _MainTex_TexelSize;
            float _Radius;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.pixel = o.uv * _MainTex_TexelSize.zw;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                half4 col = tex2D(_MainTex, i.uv);
                float2 halfSize = _MainTex_TexelSize.zw*0.5;
                float2 center = halfSize;
                halfSize -= _Radius;

                half smooth = 0.9;

                float2 pixOffset = abs(i.pixel - center) - halfSize;
                half a = 1 - step(0, pixOffset.x) * step(0, pixOffset.y);
                a += smoothstep(length(pixOffset),length(pixOffset)+smooth,_Radius);
                a = smoothstep(0.01,0.04,a);
                col.rgba *= a;
                return col;
            }
            ENDCG
        }
    }
}
