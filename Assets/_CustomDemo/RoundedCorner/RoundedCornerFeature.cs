using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class RoundedCornerFeature : ScriptableRendererFeature
{
    class CustomRenderPass : ScriptableRenderPass
    {
        readonly string renderTag = "RoundedCorner";
        Material _roundedCornerMat;
        int tempRT;
        RenderTargetIdentifier targetRT;
        CommandBuffer cmd;
        public CustomRenderPass(Shader shader)
        {
            _roundedCornerMat = CoreUtils.CreateEngineMaterial(shader);
            tempRT = Shader.PropertyToID("RoundedCornerTemp");
        }
        public void SetUp(float radius, RenderTargetIdentifier targetRT)
        {
            this.targetRT = targetRT;
            _roundedCornerMat.SetFloat("_Radius", radius);
        }
        // This method is called before executing the render pass.
        // It can be used to configure render targets and their clear state. Also to create temporary render target textures.
        // When empty this render pass will render to the active camera render target.
        // You should never call CommandBuffer.SetRenderTarget. Instead call <c>ConfigureTarget</c> and <c>ConfigureClear</c>.
        // The render pipeline will ensure target setup and clearing happens in a performant manner.
        public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData)
        {
        }
        // Here you can implement the rendering logic.
        // Use <c>ScriptableRenderContext</c> to issue drawing commands or execute command buffers
        // https://docs.unity3d.com/ScriptReference/Rendering.ScriptableRenderContext.html
        // You don't have to call ScriptableRenderContext.submit, the render pipeline will call it at specific points in the pipeline.
        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            cmd = CommandBufferPool.Get(renderTag);
            int w = renderingData.cameraData.camera.scaledPixelWidth;
            int h = renderingData.cameraData.camera.scaledPixelHeight;
            cmd.GetTemporaryRT(tempRT, w, h, 0, FilterMode.Point, RenderTextureFormat.Default);
            cmd.Blit(targetRT, tempRT, _roundedCornerMat);
            cmd.Blit(tempRT, targetRT);
            cmd.ReleaseTemporaryRT(tempRT);
            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        // Cleanup any allocated resources that were created during the execution of this render pass.
        public override void OnCameraCleanup(CommandBuffer cmd)
        {
        }

    }

    public Shader shader;
    [Range(10,150)]
    public float radius = 30;

    CustomRenderPass m_ScriptablePass;

    /// <inheritdoc/>
    public override void Create()
    {
        m_ScriptablePass = new CustomRenderPass(shader);

        // Configures where the render pass should be injected.
        m_ScriptablePass.renderPassEvent = RenderPassEvent.AfterRendering;
    }

    // Here you can inject one or multiple render passes in the renderer.
    // This method is called when setting up the renderer once per-camera.
    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        RenderTargetIdentifier targetRT;
        if(renderingData.postProcessingEnabled)
        {
            targetRT = new RenderTargetIdentifier(Shader.PropertyToID("_AfterPostProcessTexture"));
        }
        else
        {
            targetRT = renderer.cameraColorTarget;
        }
        m_ScriptablePass.SetUp(radius, targetRT);
        renderer.EnqueuePass(m_ScriptablePass);
    }
}


